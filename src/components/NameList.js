import React from 'react'
import Person from './Person'

function NameList() {
    const names = ['Bruce', 'Clark', 'Diana'];
    const nameList = names.map((name,index)=><h2 key={index}>{index} {name}</h2>)
    return (<div> { nameList } </div>
    )

    // const persons = [
    //     {
    //         id: 1,
    //         name: 'Bruce',
    //         age: 31,
    //         skill: 'React'
    //     },
    //     {
    //         id: 2,
    //         name: 'Clark',
    //         age: 30,
    //         skill: 'Angular'
    //     },
    //     {
    //         id: 3,
    //         name: 'Bruce',
    //         age: 29,
    //         skill: 'Vue'
    //     }
    // ]
    // const personsList = persons.map(person => (
    //     <Person key={person.id} person={person}/>
    // ))

    // return <div>{personsList}</div>
}

export default NameList
