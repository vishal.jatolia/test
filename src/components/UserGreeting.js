import React, { Component } from 'react'

class UserGreeting extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             isLoggedIn : true
        }
    }
    
    render() {

        // short circuit
        return this.state.isLoggedIn && <div>Hello Vishal</div>

        // ternary
        // return(
        //     this.state.isLoggedIn ?
        //     <div>Hello Vishal</div> :
        //     <div>Hello Guest</div>
        // )

        // if else
        // let message
        // if(this.state.isLoggedIn) {
        //     message = <div>Hello Vishal</div>
        // } else {
        //     message = <div>Hello Guest</div>
        // }
        // return <div>{message}</div>
        
        // if (this.state.isLoggedIn) {
        //     return <div>Hello Vishal</div>
        // } else {
        //     return <div>Hello Guest</div>
        // }

        // return (
        //     <div>
        //         <div>Hello Vishal</div>
        //         <div>Hello Guest</div>
        //     </div>
        // )
    }
}

export default UserGreeting
