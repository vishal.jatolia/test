import React from 'react'

// function Greet(){
//     return <h1>Hello Vishal</h1>
// }

// const Greet = props => {
//     return (
//         <div>
//             <h1>Hello {props.name} aka {props.heroname}</h1>
//             {props.children}
//         </div>
//     )
// }
  
// const Greet = ({name, heroname}) => {
//     return (
//         <div>
//             <h1>Hello {name} aka {heroname}</h1>
//             {/* {children} */}
//         </div>
//     )
// }

const Greet = (props) => {
    const {name, heroname, children} = props;
    return (
        <div>
            <h1>Hello {name} aka {heroname}</h1>
            {children}
        </div>
    )
}

export default Greet