import React from 'react'

// const UpdatedComponent = OriginalComponent => {
//     class NewComponent extends React.Component {
const withCounter = (WrappedComponent, incrementNum) => {
    class WithCounter extends React.Component {
        constructor(props) {
            super(props)
        
            this.state = {
                 count : 0
            }
        }
        
        incrementCount = () => {
            this.setState(prevState=> {
                return { count: prevState.count + incrementNum }
            })
        }

        render() {
            // return <OriginalComponent name='Vishal' count={this.state.count} incrementCount={this.incrementCount}/>
            return <WrappedComponent 
                    count={this.state.count} 
                    incrementCount={this.incrementCount}
                    {...this.props}
                    />
        }
    }
    // return NewComponent
    return WithCounter
}

// export default UpdatedComponent
export default withCounter