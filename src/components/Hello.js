import React from 'react'

const Hello = () => {
    // JSX
    return(
        <div className="dummyclass">
            <h1>Hello World</h1>
        </div>
    )

    // without JSX
    return React.createElement(
        'div', 
        {id : 'hello', className : 'dummyclass'}, 
        React.createElement('h1', null, 'Hello World')
    )
}

export default Hello