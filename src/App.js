import logo from './logo.svg';
import './App.css';
import Greet from './components/Greet'
import Welcome from './components/Welcome'
import Hello from './components/Hello'
import Message from './components/Message'
import Counter from './components/Counter'
import FunctionClick from './components/FunctionClick'
import ClassClick from './components/ClassClick'
import EventBind from './components/EventBind'
import ParentComponet from './components/ParentComponet'
import UserGreeting from './components/UserGreeting'
import NameList from './components/NameList'
import Stylesheet from './components/Stylesheet'
import Inline from './components/Inline'
import './appStyles.css'
import styles from './appStyles.module.css'
import Form from './components/Form'
import LifecycleA from './components/LifecycleA'
import FragmentDemo from './components/FragmentDemo'
import Table from './components/Table'
import ParentComp from './components/ParentComp'
import RefsDemo from './components/RefsDemo'
import FocusInput from './components/FocusInput'
import FRParentInput from './components/FRParentInput'
import PortalDemo from './components/PortalDemo'
import Hero from './components/Hero'
import ErrorBoundary from './components/ErrorBoundary'
import ClickCounter from './components/ClickCounter'
import HoverCounter from './components/HoverCounter'
import ClickCounterTwo from './components/ClickCounterTwo'
import HoverCounterTwo from './components/HoverCounterTwo'
import User from './components/User'
import CounterTwo from './components/CounterTwo'
import ComponentC from './components/ComponentC'
import { UserProvider } from './components/userContext'

function App() {
  return (
    <div className="App">
    <UserProvider value="Vishal">
      <ComponentC  />
    </UserProvider>

    {/* <CounterTwo>
      {(count,incrementCount) => (
        <ClickCounterTwo count={count} incrementCount={incrementCount} /> 
      )}
    </CounterTwo>

    <CounterTwo>
      {(count,incrementCount) => (
        <HoverCounterTwo count={count} incrementCount={incrementCount} /> 
      )}
    </CounterTwo> */}

    {/* <CounterTwo 
      render={(count,incrementCount) => (
        <HoverCounterTwo count={count} incrementCount={incrementCount} /> 
      )}
    /> */}

    {/* <ClickCounterTwo />
    <HoverCounterTwo />
    <User render={(isLoggedIn) => isLoggedIn ? "Vishal" : "Guest"}/> */}

    {/* <ClickCounter name="Vishal"/>
    <HoverCounter name="Jatolia"/> */}

    {/* <ErrorBoundary>
      <Hero heroname="Batman"/>
    </ErrorBoundary>

    <ErrorBoundary>
      <Hero heroname="Superman"/>
    </ErrorBoundary>

    <ErrorBoundary>
      <Hero heroname="Joker"/>
    </ErrorBoundary> */}

    {/* <PortalDemo /> */}
    {/* <FRParentInput /> */}
    {/* <FocusInput /> */}
    {/* <RefsDemo /> */}
    {/* <ParentComp /> */}
    {/* <Table /> */}
    {/* <FragmentDemo /> */}
    {/* <LifecycleA /> */}
    {/* <Form /> */}
      {/* <h1 className='error'>Error</h1>
      <h1 className={styles.success}>Success</h1>
    <Inline /> */}
    {/* <Stylesheet primary={true}/> */}
    {/* <NameList /> */}
    {/* <UserGreeting /> */}
    {/* <ParentComponet /> */}
    {/* <EventBind /> */}
    {/* <FunctionClick />
    <ClassClick /> */}
    
      {/* <Counter />
      <Message /> */}

      {/* <Greet name="Bruce" heroname="Batman">
        <p>This is child element</p>  
      </Greet>
      <Greet name="Clark" heroname="Superman">
        <button>Action</button>  
      </Greet> 
      <Greet name="Diana" heroname="Wonder Woman" /> */}

      {/* <Welcome name="Bruce" heroname="Batman">
        <p>This is child element</p> 
      </Welcome>
      <Welcome name="Clark" heroname="Superman"/>
      <Welcome name="Diana" heroname="Wonder Woman"/> */}

      {/* <Hello /> */}

      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
          Hello World
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}


    </div>
  );
}

export default App;
